﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Web.Context;

namespace Web
{
    public static class Extensions
    {
        public static string getHashSha256(this string data)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(data);
            SHA256Managed hashstring = new SHA256Managed();
            return Encoding.ASCII.GetString(hashstring.ComputeHash(bytes));
        }
        public static string getHashSha256String(this byte[] data)
        {
            SHA256Managed hashstring = new SHA256Managed();
            var hash = hashstring.ComputeHash(data);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
                sb.Append(b.ToString("X2"));
            return sb.ToString();
        }
        public static byte[] getHashSha256(this byte[] data)
        {
            SHA256Managed hashstring = new SHA256Managed();
            var hash = hashstring.ComputeHash(data);
            return hash;
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[LocalContext.rnd.Next(s.Length)]).ToArray());
        }
        public static bool isValidMail(this string mail)
        {
            try
            {
                MailAddress m = new MailAddress(mail);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        
        }
    }
}
