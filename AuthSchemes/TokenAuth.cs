﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Web.Context;

namespace Web.AuthSchemes
{
    public class TokenAuthOptions : AuthenticationSchemeOptions
    {
        public TokenAuthOptions() { }
    }
    public class TokenAuthHandler : AuthenticationHandler<TokenAuthOptions>
    {
        DatabaseContext context;
        public TokenAuthHandler(
            IOptionsMonitor <TokenAuthOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            DatabaseContext context)
            : base(options, logger, encoder, clock)
        {
            this.context = context;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Token") || string.IsNullOrEmpty(Request.Headers["Token"]))
            {
                return AuthenticateResult.Fail("Unauthorized, no token produced");
            }

            try
            {
                return validateToken(Request.Headers["Token"]);
            } 
            catch (Exception ex)
            {
                return AuthenticateResult.Fail(ex.Message);
            }
        }

        private AuthenticateResult validateToken(string token)
        {
            var validatedToken = context.tokens.Include(p=>p.user).FirstOrDefault(t => t.@string == token);
            if (validatedToken == null)
                return AuthenticateResult.Fail("Unauthorized, invalid token");

            string[] roles;
            roles = new string[] { validatedToken.tokenType.ToString().ToLower() };

            var claims = new List<Claim>();
            claims.Add(new Claim("tokenid", validatedToken.id.ToString()));

            if (validatedToken.user != null)
                claims.Add(new Claim("userid", validatedToken.user.id.ToString()));
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new System.Security.Principal.GenericPrincipal(identity, roles);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}
