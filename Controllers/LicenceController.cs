﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicenceController : ControllerBase
    {
        DatabaseContext context;
        public LicenceController(DatabaseContext _context)
        {
            this.context = _context;
        }

        #region ADMIN
        [HttpPost("reset")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> ResetLiceseAdmin(string licenseKey)
        {
            if (string.IsNullOrEmpty(licenseKey))
                return Problem("License cant be null");
            var license = context.licenses.FirstOrDefault(p => p.key == licenseKey);
            if (license == null)
                return Problem("License not found");
            if (!license.active)
                return Problem("Key inactive");
            if (license.hwid == null)
                return Problem("Key hwid null");
            license.duration = Math.Clamp(license.duration - (long)(DateTime.Now - license.StartTime).TotalSeconds, 0, long.MaxValue);
            license.StartTime = DateTime.Now;
            license.hwid = null;
            license.active = false;
            await context.SaveChangesAsync();
            return Ok();
        }
        //Admin grant
        [HttpPost("grant")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> GrantLiceseAdmin(string username, long? seconds, string game, string driverHash)
        {
            if (string.IsNullOrEmpty(game))
                return Problem("Game cant be null");
            if (seconds == null || seconds == 0)
                return Problem("Duration cant be zero");
            var user = context.users.Include(p=>p.licenses).FirstOrDefault(p => p.email == username);
            if (user == null)
                return Problem("User not found");
            Driver driver;
            if (driverHash == null)
            {
                driver = context.drivers.OrderBy(p => p.Licences.Count).FirstOrDefault();
            }
            else
            {
                driver = context.drivers.FirstOrDefault(p => p.Hash == driverHash);
            }
            if (driver == null)
                return Problem("Driver not found");
            user.licenses.Add(new License
            {
                driver = driver,
                duration = (long)seconds,
                game = game,
                user = user
            });
            await context.SaveChangesAsync();
            return Ok();
        }
        //Admin grant
        [HttpPost("generate")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> GenerateLicenseAdmin(long? seconds, string game, string driverHash)
        {
            if (string.IsNullOrEmpty(game))
                return Problem("Game cant be null");
            if (seconds == null || seconds == 0)
                return Problem("Duration cant be zero");
            Driver driver;
            if (driverHash == null)
            {
                driver = context.drivers.OrderBy(p => p.Licences.Count).FirstOrDefault();
            }
            else
            {
                driver = context.drivers.FirstOrDefault(p => p.Hash == driverHash);
            }
            if (driver == null)
                return Problem("Driver not found");

            var license = new License
            {
                driver = driver,
                duration = (long)seconds,
                game = game,
            };
            context.licenses.Add(license);
            await context.SaveChangesAsync();
            return Ok(license.key);
        }

        [HttpGet("pauseall")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> PauseAll()
        {
            List<License> activeLincesnes = context.licenses.Where(p=>p.active).ToList();
            activeLincesnes.ForEach(p =>
            {
                if (!p.isExpired)
                {
                    long remainsDuration = p.duration - (long)(DateTime.Now - p.StartTime).TotalSeconds;
                    p.StartTime = DateTime.Now;
                    p.duration = remainsDuration;
                    p.active = false;
                }
            });
            await context.SaveChangesAsync();
            return Ok();
        }
        #endregion ADMIN

        #region TOKEN
        //Reseller grant
        [HttpPost("grantrs")]
        [Authorize(Roles = "reseller")]
        public async Task<ActionResult> GrantLiceseUser(string username, long? seconds, string driverHash)
        {
            if (seconds == null || seconds == 0)
                return Problem("Duration cant be zero");
            var tokenid = long.Parse(User.Claims.First(p => p.Type == "tokenid").Value);
            var token = context.tokens.FirstOrDefault(p => p.id == tokenid);
            if (token == null)
                return Problem("Invalid token");
            if (token.ResellerGame == null)
                return Problem("Invalid token game");
            var user = context.users.Include(p => p.licenses).FirstOrDefault(p => p.email == username);
            if (user == null)
                return Problem("User not found");
            Driver driver = null;
            if (driverHash != null)
            {
                driver = context.drivers.FirstOrDefault(p => p.Hash == driverHash);
                if (driver == null)
                    return Problem("Driver not found");
            }
            user.licenses.Add(new License
            {
                driver = driver,
                duration = (long)seconds,
                game = (string)token.ResellerGame,
                user = user
            });
            await context.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("validate")]
        public async Task<ActionResult> Validate(string licenseKey)
        {
            if (string.IsNullOrEmpty(licenseKey))
                return Problem("Key cant be null");
            var license = context.licenses.FirstOrDefault(p => p.key == licenseKey);
            if (license == null)
                return Problem("License not found", statusCode:401);
            if (license.user != null)
                return Problem("No access to license", statusCode:401);
            return Ok();
        }

        [HttpGet("activate")]
        //[Authorize(Roles = "user")]
        public async Task<ActionResult> Activate(string hwid, string key)
        {
            if (string.IsNullOrEmpty(hwid))
                return Problem("Hwid empty");
            long userid;
            License license;
            if (!long.TryParse(User.Claims.FirstOrDefault(p => p.Type == "userid")?.Value, out userid))
            {
                //User not uathorized
                license = context.licenses.FirstOrDefault(p => p.key == key);
                if (license == null)
                    return Problem("Invalid license key");
                if (license.user != null)
                    return Problem("No access to license");
            }
            else
            {
                //User authorized
                var user = context.users.Include(p => p.licenses).FirstOrDefault(p => p.id == userid);
                if (user == null) return Problem("Internal user error");
                license = user.licenses.FirstOrDefault(p => p.key == key);
                if (license == null)
                    return Problem("Invalid license key");
            }
            if (license.active)
                return Problem("License already active");
            {
                license.StartTime = DateTime.Now;
                license.hwid = hwid;
                license.active = true;
            }
            context.Update(license);
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpGet("list")]
        [Authorize(Roles = "user")]
        public ActionResult GetLicensesList(string game = null, string hwid = null, bool show_expired = false)
        {
            var userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p => p.licenses).FirstOrDefault(p => p.id == userid);
            if (user == null)
                return Problem("Internal server error");
            List<Models.License> licenses;
            if (game != null)
                licenses = user.licenses.Where(p => p.game == game).ToList();
            else
                licenses = user.licenses;
            if (!show_expired)
                licenses = licenses.Where(p => !p.isExpired).ToList();
            if (hwid != null)
                licenses = licenses.Where(p => p.hwid == hwid || string.IsNullOrEmpty(p.hwid)).ToList();
            var licensesInfo = licenses.ToList().ConvertAll(p => new { key = p.key, game = p.game, active = p.active, startTime = String.Format("{0:u}", p.StartTime), endtime = String.Format("{0:u}", p.StartTime.AddSeconds(p.duration)), expired = p.isExpired});
            return Ok(licensesInfo);
        }
        #endregion TOKEN


    }
}
