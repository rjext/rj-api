﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserdataController : ControllerBase
    {
        DatabaseContext context;


        public UserdataController(DatabaseContext _context)
        {
            this.context = _context;
        }


        [HttpPut("save")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Save(string game, string key)
        {
            if (game == null)
                return Problem("No game provided");
            string value;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                value = await reader.ReadToEndAsync();
            }

            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value))
                return Problem("Key or value not provided");
            if (key.Length > 1000)
                return Problem("Key is too long, max 1000");
            if (value.Length > 50000)
                return Problem("Value is too long, max 50000");
            var userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p => p.data).FirstOrDefault(p => p.id == userid);
            if (user == null) 
                return Problem("Internal user error");
            var data = user.data.FirstOrDefault(p => p.key == key && p.game == game);
            if (data != null)
            {
                data.value = value;
                await context.SaveChangesAsync();
            } else if (user.data.Count < 5000)
            {
                user.data.Add(new Models.GameConfig.Userdata {
                    game = (string)game,
                    key = key,
                    value = value
                });
                await context.SaveChangesAsync();
            } else
            {
                return Problem("Data storage exceeded");
            }
                
            return Ok();
        }


        [HttpGet("get")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Get(string game, string key)
        {

            if (game == null || string.IsNullOrEmpty(key))
                return Problem("No key or value provided");
            if (key.Length > 1000)
                return Problem("Key is too long, max 1000");
            var userid = long.Parse(User.Claims.First(p => p.Type == "userid").Value);
            var user = context.users.Include(p => p.data).FirstOrDefault(p => p.id == userid);
            if (user == null)
                return Problem("Internal user error");
            var data = user.data.FirstOrDefault(p => p.key == key);
            if (data == null)
                return Problem("No value for provided key",statusCode: 404);
            return Ok(data.value);
            
        }
    }
}
