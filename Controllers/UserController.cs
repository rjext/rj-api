﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        DatabaseContext context;
        public UserController(DatabaseContext _context)
        {
            this.context = _context;
        }
        #region PUBLIC
        [HttpGet("register")]
        public async Task<ActionResult> Register(string email, string password)
        {
            if (email == null)
                return Problem("Email not provided");
            if (password == null)
                return Problem("Password not provided");
            if (!email.isValidMail())
                return Problem("Invalid email");
            var user = context.users.FirstOrDefault(p => p.email == email);
            if (user != null)
                return Problem("Account with this email already registered");
            if (password.Length < 6)
                return Problem("Password is too short");

            user = new User
            {
                passHash = password.getHashSha256(),
                email = email
            };
            context.users.Add(user);
            await context.SaveChangesAsync();
            return Ok();
        }
        #endregion PUBLIC
        #region TOKEN 
        [HttpGet("confirm")]
        [Authorize(Roles ="user")]
        public async Task<ActionResult> Confirm()
        {
            return Ok();
        }

 
        [HttpPost("heartbeat")]
        public ActionResult Heartbeat([FromBody]HeartbeatQuery info, string licenceKey)
        {
            License license = context.licenses.FirstOrDefault(p => p.key == licenceKey);

            if (license == null)
                return Problem("Invalid key");

            if (license.isExpired)
                return Problem("Key expired", statusCode: 401);

            var hbinfo = LocalContext.activeUsers.FirstOrDefault(p => p.key == licenceKey);

            if (hbinfo == null)
            {
                LocalContext.activeUsers.Add(new HeartbeatInfo
                {
                    key = licenceKey,
                    reported = DateTime.Now
                });
            }
            else
                hbinfo.reported = DateTime.Now;

            return Ok();
        }
        #endregion TOKEN
    }
}
