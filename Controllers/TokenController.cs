﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        DatabaseContext context;
        public TokenController(DatabaseContext _context)
        {
            this.context = _context;
        }
        #region PUBLIC
        [HttpGet("get")]
        [AllowAnonymous]
        public async Task<ActionResult> Get(string email, string password)
        {
            if (email == null)
                return Unauthorized("No email provided");
            if (password == null)
                return Unauthorized("No password provided");
            var user = this.context.users.Include(p=>p.tokens).FirstOrDefault(p => p.email == email && p.passHash == password.getHashSha256());
            if (user == null)
                return Unauthorized("Invalid username or password");
            var token = user.tokens.FirstOrDefault(p => p.isValid() && p.tokenType == TokenType.User);
            if (token == null)
            {
                token = new Models.Token { };
                token.tokenType = TokenType.User;
                user.tokens.Add(token);
                await context.SaveChangesAsync();
            }
            return Ok(token.@string);
        }
        [HttpGet("ping")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Ping()
        {
            return Ok();
        }
        #endregion PUBLIC
        [HttpGet("grantResellerToken")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> GrantResellerToken(string game, string username)
        {
            if (string.IsNullOrEmpty(username))
                return Problem("Username cant be null");
            if (game == null)
                return Problem("Game cant be null");

            var user = context.users.Include(p => p.tokens).FirstOrDefault(p => p.email == username);
            if (user == null) return Problem("User not found");
            var token = user.tokens.FirstOrDefault(p => p.tokenType == TokenType.Reseller && p.isValid() && p.ResellerGame == game);
            if (token == null)
            {
                token = new Token();
                token.ResellerGame = game;
                token.tokenType = TokenType.Reseller;
                token.ExpireTime = DateTime.Now.AddYears(100);
                user.tokens.Add(token);
                await context.SaveChangesAsync();
            }
            return Ok(token.@string);
        }

    }
}
