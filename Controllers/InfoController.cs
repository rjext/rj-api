﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;

namespace Web.Controllers
{
    [ApiController]
    public class InfoController : ControllerBase
    {
        DatabaseContext context;
        public InfoController(DatabaseContext context)
        {
            this.context = context;
        }
        [HttpGet("/api/manifest")]
        public async Task<ActionResult> Manifest()
        {
            return Ok(new {
                client_version = context.GetVersion(),


            });
        }
        [HttpGet("/api/setversion")]
        public async Task<ActionResult> SetVersion(ulong version)
        {
            await context.SetVersion(version);
            return Ok();
        }


    }
}
