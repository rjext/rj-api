﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Web.Context;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        public ulong GetTimedKey(TimeSpan interval)
        {
            long time = (long)TimeSpan.FromTicks(DateTime.Now.ToUniversalTime().Ticks).TotalSeconds;
            time = time - time % (long)interval.TotalSeconds;

            byte[] timeBytes = BitConverter.GetBytes(time);

            List<byte> bytes = new List<byte>();
            bytes.AddRange(timeBytes);
            bytes.AddRange(Encoding.UTF8.GetBytes("Govno iz sraki"));
            byte[] hash = bytes.ToArray().getHashSha256();
            byte[] truncated = new byte[sizeof(ulong)];
            Array.Copy(hash, truncated, sizeof(ulong));
            ulong timedkey = BitConverter.ToUInt64(truncated, 0);
            return timedkey;
        }
        DatabaseContext context;
        public DriverController(DatabaseContext _context)
        {
            this.context = _context;
        }
        #region SERVICE
        [HttpPost("upload")]
        [Authorize(Roles = "service")]
        public async Task<ActionResult> UploadDriver(DriverType type, ulong? secret, CommType? commType, string hookedMethod)
        {
            var stream = new StreamReader(Request.Body).BaseStream;
            using (var ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                var byteArray = ms.ToArray();
                var driverHash = byteArray.getHashSha256String();
                if (byteArray.Length > 0)
                {
                    if (!context.drivers.Any(p => p.Hash == driverHash))
                    {
                        var driverInfo = new DriverInfo();
                        if (secret != null)
                            driverInfo.Secret = (ulong)secret;
                        if (commType != null)
                            driverInfo.commType = (CommType)commType;
                        if (hookedMethod != null)
                            driverInfo.HookedMethod = hookedMethod;
                        var driver = context.drivers.Add(new Models.Driver
                        {
                            driver = byteArray,
                            Hash = driverHash,
                            Info = driverInfo,
                            Type = type
                        });
                        await context.SaveChangesAsync();
                    }
                    return Ok(driverHash);
                }
                else 
                    return Problem("Driver empty");
            }
        }

        #endregion SERVICE
        #region USER
        [HttpGet("get")]
        //[Authorize(Roles = "user")]
        public async Task<ActionResult> GetDriver(string licenseKey, string hwid)
        {
            if (string.IsNullOrEmpty(hwid))
                return Problem("Hwid empty");
            long userid;
            License license;
            if (!long.TryParse(User.Claims.FirstOrDefault(p => p.Type == "userid")?.Value, out userid))
            {
                license = context.licenses.Include(p => p.driver).Include(p => p.user).FirstOrDefault(p => p.key == licenseKey);
            }
            else
            { 
                license = context.licenses.Include(p => p.driver).Include(p => p.user).FirstOrDefault(p => p.key == licenseKey && p.hwid == hwid && p.user.id == userid);
            }
            if (license == null)
                return Problem("License not found");
            if (!license.active)
                return Problem("License inactive", statusCode:401);

            return File(license.driver.driver, "application/octet-stream");
        }
        [HttpGet("getTime")]
        public ActionResult GetKey()
        {
            return Ok((long)((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds());
                }
        [HttpGet("getKey")]
        //[Authorize(Roles = "user")]
        public ActionResult GetKey(string licenseKey)
        {
            var license = context.licenses.Include(p => p.driver).Include(p => p.user).FirstOrDefault(p => p.key == licenseKey);
            if (license == null)
                return Problem("Invalid license");
            return Ok(GetTimedKey(TimeSpan.FromMinutes(5)));
        }
        #endregion USER
    }
}
