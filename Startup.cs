using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.AuthSchemes;
using Web.Context;
using Prometheus;
using Web.Services;
using Web.Models;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var gauge = Metrics.CreateGauge("active_users", "currently active users amount");
            Metrics.DefaultRegistry.AddBeforeCollectCallback(async (cancel) =>
            {
                gauge.Set(LocalContext.activeUsers.Count);
            });

            services.AddRoutinesHandler(p => {
                p.Add("activeUsers.clear", new Routine(TimeSpan.FromSeconds(5), () => 
                LocalContext.activeUsers.RemoveAll(itm => itm.reported.AddSeconds(20) < DateTime.Now)
                ));
            });

            string connstr = string.Format("Server={0};Port={1};Database={2};User ID={3};Password={4};Pooling=true;Connection Lifetime=0;SslMode=Disable;SslMode=Disable;",
                Configuration.GetSection("Postgres:host").Value,
                Configuration.GetSection("Postgres:port").Value,
                Configuration.GetSection("Postgres:database").Value,
                Configuration.GetSection("Postgres:user").Value,
                Configuration.GetSection("Postgres:password").Value);

            services.AddControllers();
            var builder = new DbContextOptionsBuilder<DatabaseContext>().UseNpgsql(connstr).Options;

                
            services.AddDbContext<Context.DatabaseContext>(options =>
                    options.UseNpgsql(connstr)
                    .EnableSensitiveDataLogging()
                    .EnableDetailedErrors()
                , ServiceLifetime.Scoped);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Token";
                options.DefaultChallengeScheme = "Token";
            })
            .AddScheme<TokenAuthOptions, TokenAuthHandler>("Token", (p) => {});

            services.AddAuthorization(configure => {});



            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Web", Version = "v1" });
            });
            Console.WriteLine("Startup complete");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web v1"));

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseHttpMetrics();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapMetrics();
            });
        }
    }
}
