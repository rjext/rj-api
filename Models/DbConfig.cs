﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Web.Context;

namespace Web.Models
{
    public static class DbConfigExtension
    {
        public static bool HasKey(this DatabaseContext context, string key)
        {
            return context.configs.Any(p => p.key == key);
        }
        public static void SetValue(this DatabaseContext context, string key, byte[] value)
        {
            var row = context.configs.FirstOrDefault(p => p.key == key);
            if (row == null)
                context.configs.Add(new DbConfig { key = key, value = value });
            else
                row.value = value;
        }
        public static byte[] GetValue(this DatabaseContext context, string key)
        {
            var row = context.configs.FirstOrDefault(p => p.key == key);
            if (row == null)
                return null;
            return row.value;
        }
        public static ulong GetVersion(this DatabaseContext context)
        {
            var vers = context.configs.FirstOrDefault(p => p.key == "client_version");
            if (vers == null)
                return 0;
            return BitConverter.ToUInt64(vers.value);
        }
        public static async Task SetVersion(this DatabaseContext context, ulong version)
        {
            var vers = context.configs.FirstOrDefault(p => p.key == "client_version");
            if (vers != null)
                BitConverter.GetBytes(version);
            else
                context.configs.Add(new DbConfig {key = "client_version", value = BitConverter.GetBytes(version) });
            await context.SaveChangesAsync();
        }


    }


    public class DbConfig
    {
        [Key]
        public long id { get; set; }

        [Required]
        public string key { get; set; }
        [Required]
        public byte[] value { get; set; }
    }
}
