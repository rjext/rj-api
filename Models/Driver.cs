﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public enum DriverType
    {
        Public,
        Personal,
        Private
    }
    public enum CommType
    {
        SystemHook,
        SharedMem,

    }

    public class DriverInfo
    {
        public ulong Secret { get; set; } = 0x01;

        public string HookedMethod { get; set; } = null;

        public CommType commType { get; set; } = CommType.SystemHook;
    }

    public class Driver
    {
        public long Id { get; set; }
        public byte[] driver { get; set; }



        [NotMapped]
        public DriverInfo Info
        {
            get => JsonConvert.DeserializeObject<DriverInfo>(internal_info);
            set => this.internal_info = JsonConvert.SerializeObject(value);
        }

        public string internal_info = JsonConvert.SerializeObject(new DriverInfo());

        [MaxLength(128)]
        public string Hash { get; set; }

        public DriverType Type { get; set; }

        public List<License> Licences { get; set; }
    }
}
