﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Web.Models.GameConfig
{

    public class Userdata
    { 
        [Key]
        public long id { get; set; }

        [Required]
        public string game { get; set; }

        [Required]
        public string key { get; set; }
        [Required]
        public string value { get; set; }

        public User user { get; set; } 
    }
}
