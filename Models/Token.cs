﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class Token
    {
        [Key]
        public long id { get; set; }
        public DateTime ExpireTime { get; set; } = DateTime.Now.AddDays(7);
        
        [MaxLength(128)]
        public string @string { get; set; } = Extensions.RandomString(128);

        public string ResellerGame { get; set; } = null;

        public TokenType tokenType { get; set; } = TokenType.User;
        public User user { get; set; }


        public bool isValid() => this.ExpireTime > DateTime.Now;
    }
}
