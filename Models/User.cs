﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.GameConfig;

namespace Web.Models
{
    public class User
    {
        [Key]
        public long id { get; set; }

        [MaxLength(128)]
        public string email { get; set; }

        public DateTime registerTime { get; set; } = DateTime.Now;

        public int lvl { get; set; } = 0;

        [MaxLength(8)]
        public string confirmation_code { get; set; } = Extensions.RandomString(8);
        public bool confirmed { get; set; } = false;

        [MaxLength(128)]
        public string passHash { get; set; }


        public List<License> licenses { get; set; }
        public List<Token> tokens { get; set; }

        public List<Userdata> data { get; set; }
    }
}
