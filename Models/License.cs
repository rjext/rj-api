﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Models
{
    public class License
    {
        [Key]
        public long id { get; set; }
        public long duration { get; set; }
        public DateTime StartTime { get; set; }

        public bool active { get; set; } = false;

        [MaxLength(128)]
        public string key { get; set; } = Extensions.RandomString(128);

        [MaxLength(250)]
        public string hwid { get; set; } = null;

        public string game { get; set; }

        public User user { get; set; }
        public Driver driver { get; set; }

        public bool isExpired => (active && DateTime.Now > StartTime.AddSeconds(duration));

    }
}
