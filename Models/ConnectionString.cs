﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class ConnectionString
    {
        public string host { get; set; }
        public int port { get; set; }

        public string database { get; set; }

        public string user { get; set; }
        public string password { get; set; }

        public string Result => $"Server={host};Port={port};Database={database};User ID={user};Password={password};Pooling=true;Connection Lifetime=0;";
    }

}
