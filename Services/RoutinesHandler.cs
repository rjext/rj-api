﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Services
{
    public static class RoutinesHandlerExtension
    {
        public static void AddRoutinesHandler(this IServiceCollection collection, Action<RoutinesHandler> routinesBuilder)
        {
            var service = new RoutinesHandler(routinesBuilder);
            collection.AddSingleton(service);
        }
    }
    public class RoutinesHandler
    {
        private Dictionary<string, Task> tasks;
        public RoutinesHandler(Action<RoutinesHandler> routinesBuilder)
        {
            tasks = new Dictionary<string, Task>();
            routinesBuilder.Invoke(this);
        }
        public Task Add(string key, Routine routine)
        {
            var task = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    routine.task.Invoke();
                    Thread.Sleep((int)routine.interval.TotalMilliseconds);
                }
            });
            tasks.Add(key, task);
            return task;
        }
        /*
        public void Stop(string key)
        {
            var task = tasks.FirstOrDefault(p => p.Key == key);
            if (task != null)
                task.Value.
        }
        */
    }
}
