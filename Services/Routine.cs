﻿using Npgsql.TypeHandlers.DateTimeHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Services
{
    public class Routine
    {
        public TimeSpan interval { get; set; }
        public Action task { get; set; }
        public Routine(TimeSpan interval, Action task)
        {
            this.interval = interval;
            this.task = task;
        }
    }
}
