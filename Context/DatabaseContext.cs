﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;
using Web.Models.GameConfig;

namespace Web.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            if (Database.EnsureCreated())
            {
                if (!tokens.Any(p => p.tokenType == Models.TokenType.Service))
                    tokens.Add(new Models.Token { tokenType = Models.TokenType.Service, ExpireTime = DateTime.MaxValue });
                if (this.GetVersion() == 0)
                    this.SetVersion(1);
                SaveChanges();
            }
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableDetailedErrors();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.User>()
                .HasMany(p => p.licenses)
                .WithOne(p => p.user);
            modelBuilder.Entity<Models.User>()
                .HasMany(p => p.tokens)
                .WithOne(p => p.user);
            modelBuilder.Entity<Models.User>()
                .HasMany(p => p.data)
                .WithOne(p => p.user);

            modelBuilder.Entity<Models.Driver>()
                .HasMany(p => p.Licences)
                .WithOne(p => p.driver);


            modelBuilder.Entity<Models.License>().HasIndex(u => u.key).IsUnique();

            modelBuilder.Entity<Models.DbConfig>().HasIndex(u => u.key).IsUnique();
        }

        public DbSet<User> users { get; set; }
        public DbSet<Token> tokens { get; set; }
        public DbSet<Driver> drivers { get; set; }
        public DbSet<License> licenses { get; set; }
        public DbSet<Userdata> userdata { get; set; }
        public DbSet<DbConfig> configs { get; set; }

    }
}
