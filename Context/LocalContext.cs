﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Context
{
    public static class LocalContext
    {
        public static Random rnd = new Random((int)(DateTime.Now.Ticks%int.MaxValue));
        public static List<HeartbeatInfo> activeUsers = new List<HeartbeatInfo>();
    }
}
